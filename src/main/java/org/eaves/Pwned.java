package org.eaves;


import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class Pwned {

    private static final String URL = "https://api.pwnedpasswords.com/pwnedpassword/";
    private final static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    @Parameter (names = { "--file", "-f"})
    private static String _fileName = "somefile.txt";
    
    @Parameter (names = { "-n"})
    private static boolean _noCallAPI = false;
    
    @Parameter (names = { "--head", "-h"})
    private static int _head = Integer.MAX_VALUE;
    
    @Parameter (names = { "--quiet", "-q"})
    private static boolean _quiet = false;
    
    public static void main(String ... argv) throws Exception {
        Pwned pwned = new Pwned();
        JCommander.newBuilder().addObject(pwned).build().parse(argv);
        pwned.run(_fileName);
    }

    private void run(String file) throws Exception {
        if (_noCallAPI){
            System.err.println("*** NOT CALLING API due to '-n' argument on command line ****. Checking File ONLY");
        }

        MessageDigest digest = null;
        digest = MessageDigest.getInstance("SHA-1");

        if (!_quiet) {
            System.err.println("Looking for file : " + Paths.get(file).toAbsolutePath().normalize().toString());
        }

        List<Account> all = processFile(file);

        int processed = 0;
        for (Account a : all) {
            ++processed;
            
            if (processed < _head) {
                if (!_quiet) {
                    System.err.print(".");
                }
                byte[] sha = digest.digest(a.passwordBytes());
                if (!_noCallAPI) {
                    boolean rv = checkAgainstDatabase(hex(sha));
                    if (rv) {
                        System.out.println(a.toString());
                    }
                }
            }
        }
        
    }

    private String hex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    private ByteArrayOutputStream readBody(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos;
    }

    private boolean checkAgainstDatabase(String hex) throws IOException {

        URL obj = new URL(URL+hex);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", "org.eaves.Pwned");

        int responseCode = con.getResponseCode();
        // System.out.println("\nSending 'GET' request to URL : " + url);
        // System.out.println("Response Code : " + responseCode);   
        switch (responseCode) {
            case 200:
                String count = readBody(con.getInputStream()).toString("UTF-8");
                
            case 404:
                break;
            case 400:
                System.err.println("The data passed does not conform to the API specifications [" + hex + "]");
                break;
            case 403:
                System.err.println("Forbidden: No user agent in the request");
                break;
            case 429:
                System.err.println("RATE LIMITED - THIS SHOULD NOT BE HAPPENING - SLEEPING LONGER");
                try {
                    Thread.sleep(10000); // sleep additional 10 seconds
                } catch (InterruptedException e) {
                    // don't care
                }
                break;
            default:
                System.err.println("Unknown response code " + responseCode);
        }
        
        return responseCode == 200;
    }
    

    private List<Account> processFile(String file) throws IOException {
        FileFormat f = identifyFile(file);
        return loadFile(file, f);
    }

    private FileFormat identifyFile(String file) throws IOException {
        CSVReader reader = new CSVReaderBuilder(new FileReader(file)).build();
        FileFormat f = new FileFormat(reader.readNext());
        return f;
    }

    private List<Account> loadFile(String file, FileFormat f) throws IOException {
        List<Account> accounts = new ArrayList<>();
        
        Account a = new Account("XKCD-TRUE", "https://xkcd.com/936/", "trombonetroubador", "correcthorsebatterystaple");
        accounts.add(a);
        
        CSVReader reader = new CSVReaderBuilder(new FileReader(file))
                .withSkipLines(1)
                .build();
        
        String [] nextLine;
        int fieldCount = f.fieldCount();
        int lineCount = 1;  // starts at 1 - skips the first line as 1Password generates a heading
        while ((nextLine = reader.readNext()) != null) {
            if (nextLine.length == fieldCount) {
                for (int i=0;i<fieldCount;i++) {
                    if (nextLine[i] == null)
                    {
                        System.err.println("CSVReader returned an element in the line of null, this shouldn't happen, the CSV is probably not correct in some way");
                        System.err.println("---- line number : " + lineCount);
                        System.err.println("Processing will be aborted");
                        System.exit(1);
                    }
                }

                a = new Account(nextLine[f.position("title")], nextLine[f.position("url")], 
                        nextLine[f.position("username")], nextLine[f.position("password")]);
                accounts.add(a);
                ++lineCount;
            }
            else
            {
                System.err.println("CSVReader found " + nextLine.length + " elements in line " + lineCount + ". Should be exactly "+fieldCount);
                System.err.println("Processing will be aborted");
                System.exit(1);
            }
        }

        return accounts;
    }
}
