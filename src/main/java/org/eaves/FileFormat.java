package org.eaves;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileFormat {
    private int _n;
    private List<String> _fields;
    
    public FileFormat(String[] columns)
    {
        _n = columns.length;
        
        _fields = Arrays.asList(columns).stream()
                .map(String::toLowerCase)
                .map(str -> str.replaceAll("[^\\x00-\\x7F]",""))
                .collect(Collectors.toList());
    }
    
    public int position(String column)
    {
        int index = _fields.indexOf(column.toLowerCase());
        
        if (index < 0) 
        {
            System.err.println("Cannont find " + column + " in " + _fields);
        }
        return index;
    }

    public int fieldCount() {
        return _n;
    }
}
