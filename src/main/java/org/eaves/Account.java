package org.eaves;

import java.io.UnsupportedEncodingException;

class Account {

    private String _title;
    private String _url;
    private String _username;
    private String _password;   // this is the UTF-8 SHA1 encoded version
    
    Account(String title, String url, String username, String pw){
        _title = title;
        _url = url;
        _username = username;
        _password = (pw == null)?"":pw;  // clean this up
    }


    @Override
    public String toString() {
        return "A: <" + _title + " : " + _url + " : " + _username + ">";
    }
    
    public byte[] passwordBytes() {
        byte[] rv = new byte[20];
        try {
            rv =  _password.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.err.println("For some reason your environment doesn't support UTF-8, let's try the default - good luck");
            rv = _password.getBytes();
        }
        return rv;
    }
}
